import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class DataService {

  constructor(private http: HttpClient) {}

  getData(site: string) {
    return this.http.get<number[][]>(`assets/data/${site}`);
  }
}

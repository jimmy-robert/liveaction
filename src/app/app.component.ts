import { Component } from '@angular/core';
import { ChartPoint } from 'chart.js';
import { DataService } from './services/data.service';

const INITIAL_VALUE = 'SiteA.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'liveaction';

  selectedOption = INITIAL_VALUE;
  appliedOption = INITIAL_VALUE;

  // tslint:disable-next-line:max-line-length
  data: ChartPoint[] = [];

  constructor(private dataService: DataService) {
    this.fetchData();
  }

  onCancel() {
    this.selectedOption = this.appliedOption;
  }

  onApply() {
    this.appliedOption = this.selectedOption;
    this.fetchData();
  }

  private fetchData() {
    this.dataService.getData(this.appliedOption).subscribe(
      (values) => {
        this.data = values.map((point): ChartPoint => {
          return {
            x: point[0],
            y: point[1],
          };
        });
        console.log(JSON.stringify(this.data));
      },
      (error) => console.error(error)
    );
  }
}

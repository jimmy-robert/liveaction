import { Component, Input } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartPoint } from 'chart.js';
import * as moment from 'moment';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss']
})
export class GraphComponent {

  @Input()
  title = '';

  @Input()
  color = '';

  @Input()
  backgroundColor = '';

  @Input()
  set data(points: ChartPoint[]) {
    this.chartData = [
      {
        data: points,
        label: '(AVC) Traffic out',
        lineTension: 0,
        borderColor: this.color,
        backgroundColor: this.backgroundColor,
        showLine: true,
        pointRadius: 0,
      },
    ];
  }

  public chartData: ChartDataSets[] = [];

  public chartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'bottom',
      labels: {
      }
    },
    scales: {
      xAxes: [
        {
          ticks: {
            stepSize: 1000,
            callback(value) {
              return moment(value).format('mm:ss');
            }
          }
        },
      ],
      yAxes: [
        {
          ticks: {
            suggestedMin: 0,
            suggestedMax: 40000000,
            stepSize: 20000000,
            callback(value) {
              const val = value / 1000 / 1000;
              return val.toFixed(1) + ' Mo';
            }
          }
        },
      ]
    }
  };

}
